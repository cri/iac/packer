version = "1.0.0"

source_image_id = "9978afe0-caa0-4c17-b5c4-aff0963f277f" #  Ubuntu Server 22.04.3
builder_flavor  = "c1.large"
networks_id = [
  "bb76f5bf-1542-42df-8948-d8f2e054024f" # admin-svc-net
]

security_groups_id = [
  "2151e267-f9f8-48d7-8114-f69ef9357bb7",
  "6ff96079-b319-402b-ba0c-c5e36c4354ba",
  "a18c1b1b-08d9-4f68-880b-e64a8293ce39",
  "fe922032-ffc1-4566-bc3a-5818d662e8cb",
]

ssh_bastion_host     = "bastion.cri-playground.iaas.epita.fr."
ssh_bastion_username = "root"
