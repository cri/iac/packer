{
  description = "Forge Packer images";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils = {
      url = "github:numtide/flake-utils";
      inputs = {
        nixpkgs.follows = "nixpkgs";
      };
    };
    pre-commit-hooks = {
      url = "github:cachix/pre-commit-hooks.nix";
      inputs = {
        nixpkgs-stable.follows = "nixpkgs";
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };
  };

  outputs = { self, nixpkgs, flake-utils, pre-commit-hooks }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        hook = pre-commit-hooks.lib.${system};
        pre-commit-check = pre-commit-hooks.checks.${system};
      in
      rec {
        checks.pre-commit-check = hook.run {
          src = ./.;
          hooks = {
            ansible-lint.enable = true;
            markdownlint.enable = true;
            nixpkgs-fmt.enable = true;
            packer-fmt = {
              enable = true;
              name = "Packer Format";
              entry = builtins.toString (pkgs.writeShellScript "packer-fmt" ''
                export error=0
                for file in "$@"; do
                    ${pkgs.packer}/bin/packer fmt --check --diff "$file" \
                    && echo "$file" \
                    || export error=1
                done
                exit "$error"
              '');
              files = "\\.(pkrvars|pkr)\\.hcl$";
              types = [ "text" ];
            };
            yamllint.enable = true;
          };
        };

        devShell = pkgs.mkShell {
          name = "Forge-Packer";

          shellHook = ''
            ${checks.pre-commit-check.shellHook}
            . ./config.sh
          '';

          buildInputs = with pkgs; [
            packer

            vault
            python3Packages.hvac
            openstackclient
            ansible
          ];

          packages = with pkgs; [
            rnix-lsp

            pre-commit-check.ansible-lint
            pre-commit-check.markdownlint-cli
            pre-commit-check.nixpkgs-fmt
            pre-commit-check.yamllint
          ];
        };
      });
}
