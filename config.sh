export VAULT_ADDR=https://vault.cri.epita.fr
export OS_AUTH_URL=https://openstack.cri.epita.fr:5000/v3/
export OS_DOMAIN_ID=c9076a97db7b46b7ba5a8cf41af02c3b # CRI
export OS_REGION_NAME="RegionOne"
export OS_TENANT_ID=48d9938a987d4ab684a489b294b83017 # CRI_Playground
