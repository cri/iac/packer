echo
echo -n "Openstack Username: "
read os_username
echo -n "Openstack Password: "
read -s os_password
echo

vault login -no-print -method=ldap username="$os_username" password="$os_password"

export OS_USERNAME="$os_username"
export OS_PASSWORD="$os_password"
