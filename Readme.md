# Packer

this repositry containe every packer images

## Dependencies

- packer
- openstackclient
- ansible

## Usage

You need to use your credential to build the images

```sh
nix develop
# or
source config.sh
```

init packer

```sh
packer init .
```

build images:

```sh
packer build .
```

## Configuration

### Maas worker

you can find the configuration under `maas.auto.pkrvars.hcl`

- `version`: the semantic version of the maas worker
